# Translations template for bridgedb.
# Copyright (C) 2020 'The Tor Project, Inc.'
# This file is distributed under the same license as the bridgedb project.
# 
# Translators:
# khi, 2013
# khi, 2013
# khi, 2013
# Christopher M <i@cicku.me>, 2012
# Cloud P <heige.pcloud@outlook.com>, 2019
# ff98sha, 2019-2020
# hanl <iamh4n@gmail.com>, 2011
# Meng San, 2014,2016
# leungsookfan <leung.sookfan@riseup.net>, 2014
# Meng San, 2016
# Mingye Wang <arthur200126@gmail.com>, 2016
# Scott Rhodes <starring169@gmail.com>, 2020
# khi, 2014-2015
# YFdyh000 <yfdyh000@gmail.com>, 2014-2015
# ff98sha, 2019
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: 'https://trac.torproject.org/projects/tor/newticket?component=BridgeDB&keywords=bridgedb-reported,msgid&cc=isis,sysrqb&owner=isis'\n"
"POT-Creation-Date: 2020-05-14 14:21-0700\n"
"PO-Revision-Date: 2020-05-15 08:24+0000\n"
"Last-Translator: Transifex Bot <>\n"
"Language-Team: Chinese (China) (http://www.transifex.com/otf/torproject/language/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. -*- coding: utf-8 ; test-case-name: bridgedb.test.test_https_server -*-
#. This file is part of BridgeDB, a Tor bridge distribution system.
#. :authors: please see included AUTHORS file
#. :copyright: (c) 2007-2017, The Tor Project, Inc.
#. (c) 2013-2017, Isis Lovecruft
#. :license: see LICENSE for licensing information
#. : The path to the HTTPS distributor's web templates.  (Should be the
#. : "templates" directory in the same directory as this file.)
#. Setting `filesystem_checks` to False is recommended for production servers,
#. due to potential speed increases. This means that the atimes of the Mako
#. template files aren't rechecked every time the template is requested
#. (otherwise, if they are checked, and the atime is newer, the template is
#. recompiled). `collection_size` sets the number of compiled templates which
#. are cached before the least recently used ones are removed. See:
#. http://docs.makotemplates.org/en/latest/usage.html#using-templatelookup
#. : A list of supported language tuples. Use getSortedLangList() to read this
#. variable.
#. We use our metrics singleton to keep track of BridgeDB metrics such as
#. "number of failed HTTPS bridge requests."
#. Convert all key/value pairs from bytes to str.
#. TRANSLATORS: Please DO NOT translate the following words and/or phrases in
#. any string (regardless of capitalization and/or punctuation):
#. "BridgeDB"
#. "pluggable transport"
#. "pluggable transports"
#. "obfs4"
#. "Tor"
#. "Tor Browser"
#: bridgedb/distributors/https/server.py:154
msgid "Sorry! Something went wrong with your request."
msgstr "抱歉，你的邮件请求出现问题。"

#: bridgedb/distributors/https/templates/base.html:42
msgid "Language"
msgstr "语言"

#: bridgedb/distributors/https/templates/base.html:94
msgid "Report a Bug"
msgstr "报告漏洞"

#: bridgedb/distributors/https/templates/base.html:97
msgid "Source Code"
msgstr "源代码"

#: bridgedb/distributors/https/templates/base.html:100
msgid "Changelog"
msgstr "更改日志"

#: bridgedb/distributors/https/templates/bridges.html:35
msgid "Select All"
msgstr "选择全部"

#: bridgedb/distributors/https/templates/bridges.html:40
msgid "Show QRCode"
msgstr "显示二维码"

#: bridgedb/distributors/https/templates/bridges.html:52
msgid "QRCode for your bridge lines"
msgstr "网桥二维码"

#: bridgedb/distributors/https/templates/bridges.html:63
msgid "It seems there was an error getting your QRCode."
msgstr "获取二维码时出错。"

#: bridgedb/distributors/https/templates/bridges.html:68
msgid ""
"This QRCode contains your bridge lines. Scan it with a QRCode reader to copy"
" your bridge lines onto mobile and other devices."
msgstr "二维码包含网桥信息。利用二维码扫描程序，可将相应的网桥信息复制到手机或其他设备。"

#: bridgedb/distributors/https/templates/bridges.html:110
msgid "BridgeDB encountered an error."
msgstr ""

#: bridgedb/distributors/https/templates/bridges.html:116
msgid "There currently aren't any bridges available..."
msgstr "现在没有可用的网桥。"

#: bridgedb/distributors/https/templates/bridges.html:118
#: bridgedb/distributors/https/templates/bridges.html:122
#, python-format
msgid ""
" Perhaps you should try %s going back %s and choosing a different bridge "
"type!"
msgstr "试试 %s返回%s到前一页面，然后选择其他类型的网桥。"

#: bridgedb/distributors/https/templates/index.html:11
#, python-format
msgid "Step %s1%s"
msgstr "第 %s 1 %s 步"

#: bridgedb/distributors/https/templates/index.html:13
#, python-format
msgid "Download %s Tor Browser %s"
msgstr "下载 %s Tor 浏览器 %s"

#: bridgedb/distributors/https/templates/index.html:25
#, python-format
msgid "Step %s2%s"
msgstr "第 %s 2 %s 步"

#: bridgedb/distributors/https/templates/index.html:28
#: bridgedb/distributors/https/templates/index.html:30
#, python-format
msgid "Get %s bridges %s"
msgstr "获取 %s 网桥 %s"

#: bridgedb/distributors/https/templates/index.html:40
#, python-format
msgid "Step %s3%s"
msgstr "第 %s 3 %s 步"

#: bridgedb/distributors/https/templates/index.html:43
#: bridgedb/distributors/https/templates/index.html:47
#, python-format
msgid "Now %s add the bridges to Tor Browser %s"
msgstr "如何 %s 在 Tor 浏览器添加网桥%s"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. (These are used to insert HTML5 underlining tags, to mark accesskeys
#. for disabled users.)
#: bridgedb/distributors/https/templates/options.html:42
#, python-format
msgid "%sJ%sust give me bridges!"
msgstr " 直接给我网桥(%sJ%s)！ "

#: bridgedb/distributors/https/templates/options.html:55
msgid "Advanced Options"
msgstr "高级选项"

#: bridgedb/distributors/https/templates/options.html:93
msgid "No"
msgstr "否"

#: bridgedb/distributors/https/templates/options.html:94
msgid "none"
msgstr "无"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Translate "Yes!" as in "Yes! I do need IPv6 addresses."
#: bridgedb/distributors/https/templates/options.html:131
#, python-format
msgid "%sY%ses!"
msgstr "是(%sY%s)！"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Please do NOT translate the word "bridge"!
#: bridgedb/distributors/https/templates/options.html:154
#, python-format
msgid "%sG%set Bridges"
msgstr "获取网桥(%sG%s)"

#: bridgedb/strings.py:33
msgid "[This is an automated email.]"
msgstr "[这是一份自动发送的邮件。]"

#: bridgedb/strings.py:35
msgid "Here are your bridges:"
msgstr "以下是为你提供的网桥："

#: bridgedb/strings.py:37
#, python-format
msgid ""
"You have exceeded the rate limit. Please slow down! The minimum time between\n"
"emails is %s hours. All further emails during this time period will be ignored."
msgstr "您已超出了发送频率的限制，请慢慢来！两封邮件之间需要最少 %s 小时的间隔。在间隔期间发出的所有邮件将被忽略。"

#: bridgedb/strings.py:40
msgid ""
"If these bridges are not what you need, reply to this email with one of\n"
"the following commands in the message body:"
msgstr "如果这些网桥不是你所需要的，回复该邮件并在信息的主体部分包含如下的命令："

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#. TRANSLATORS: Please DO NOT translate "Tor".
#. TRANSLATORS: Please DO NOT translate "Tor Network".
#: bridgedb/strings.py:50
#, python-format
msgid ""
"BridgeDB can provide bridges with several %stypes of Pluggable Transports%s,\n"
"which can help obfuscate your connections to the Tor Network, making it more\n"
"difficult for anyone watching your internet traffic to determine that you are\n"
"using Tor.\n"
"\n"
msgstr "BridgeDB 提供多种%s可拔插传输%s网桥，用于混淆 Tor 网络的连接，从而让网络监控者难以判断你在使用 Tor。\n"

#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#: bridgedb/strings.py:57
msgid ""
"Some bridges with IPv6 addresses are also available, though some Pluggable\n"
"Transports aren't IPv6 compatible.\n"
"\n"
msgstr "另外提供一些IPv6格式的网桥，不过某些可插拔传输类型目前还不支持IPv6。\n"

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: The phrase "plain-ol'-vanilla" means "plain, boring,
#. regular, or unexciting". Like vanilla ice cream. It refers to bridges
#. which do not have Pluggable Transports, and only speak the regular,
#. boring Tor protocol. Translate it as you see fit. Have fun with it.
#: bridgedb/strings.py:66
#, python-format
msgid ""
"Additionally, BridgeDB has plenty of plain-ol'-vanilla bridges %s without any\n"
"Pluggable Transports %s which maybe doesn't sound as cool, but they can still\n"
"help to circumvent internet censorship in many cases.\n"
"\n"
msgstr "此外，BridgeDB 提供很多 %s 非可插拔传输 %s 的普通网桥。\n虽然听起来不够酷，但是这些普通网桥依然可以在很多情况下帮助绕过审查。\n"

#: bridgedb/strings.py:78 bridgedb/test/test_https.py:356
msgid "What are bridges?"
msgstr "什么是网桥？"

#: bridgedb/strings.py:79
#, python-format
msgid "%s Bridges %s are Tor relays that help you circumvent censorship."
msgstr "%s网桥%s即 Tor 中继节点，用于帮助用户绕过审查或封锁。"

#: bridgedb/strings.py:84
msgid "I need an alternative way of getting bridges!"
msgstr "需要使用其他获取方式获取网桥！"

#. TRANSLATORS: Please DO NOT translate "get transport obfs4".
#: bridgedb/strings.py:86
#, python-format
msgid ""
"Another way to get bridges is to send an email to %s. Leave the email subject\n"
"empty and write \"get transport obfs4\" in the email's message body. Please note\n"
"that you must send the email using an address from one of the following email\n"
"providers: %s or %s."
msgstr "另一种获取网桥的方式是向 %s 发送邮件。\n将邮件主题留空，并在邮件正文写“get transport obfs4”。\n请注意：你必须使用下列邮件服务商\n提供的邮箱发送邮件：%s 或 %s。"

#: bridgedb/strings.py:94
msgid "My bridges don't work! I need help!"
msgstr "用网桥也无法连接，需要帮助！"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#. TRANSLATORS: The two '%s' are substituted with "Tor Browser Manual" and
#. "Support Portal", respectively.
#: bridgedb/strings.py:98
#, python-format
msgid ""
"If your Tor Browser cannot connect, please take a look at the %s and our %s."
msgstr "如果您的 Tor 浏览器无法连接，请查看%s和%s。"

#: bridgedb/strings.py:102
msgid "Here are your bridge lines:"
msgstr "以下是为你提供的网桥："

#: bridgedb/strings.py:103
msgid "Get Bridges!"
msgstr "获得网桥！"

#: bridgedb/strings.py:107
msgid "Bridge distribution mechanisms"
msgstr "网桥分发机制"

#. TRANSLATORS: Please DO NOT translate "BridgeDB", "HTTPS", and "Moat".
#: bridgedb/strings.py:109
#, python-format
msgid ""
"BridgeDB implements four mechanisms to distribute bridges: \"HTTPS\", \"Moat\",\n"
"\"Email\", and \"Reserved\".  Bridges that are not distributed over BridgeDB use\n"
"the pseudo-mechanism \"None\".  The following list briefly explains how these\n"
"mechanisms work and our %sBridgeDB metrics%s visualize how popular each of the\n"
"mechanisms is."
msgstr ""

#: bridgedb/strings.py:115
#, python-format
msgid ""
"The \"HTTPS\" distribution mechanism hands out bridges over this website.  To get\n"
"bridges, go to %sbridges.torproject.org%s, select your preferred options, and\n"
"solve the subsequent CAPTCHA."
msgstr ""

#: bridgedb/strings.py:119
#, python-format
msgid ""
"The \"Moat\" distribution mechanism is part of Tor Browser, allowing users to\n"
"request bridges from inside their Tor Browser settings.  To get bridges, go to\n"
"your Tor Browser's %sTor settings%s, click on \"request a new bridge\", solve the\n"
"subsequent CAPTCHA, and Tor Browser will automatically add your new\n"
"bridges."
msgstr ""

#: bridgedb/strings.py:125
#, python-format
msgid ""
"Users can request bridges from the \"Email\" distribution mechanism by sending an\n"
"email to %sbridges@torproject.org%s and writing \"get transport obfs4\" in the\n"
"email body."
msgstr ""

#: bridgedb/strings.py:129
msgid "Reserved"
msgstr ""

#: bridgedb/strings.py:130
#, python-format
msgid ""
"BridgeDB maintains a small number of bridges that are not distributed\n"
"automatically.  Instead, we reserve these bridges for manual distribution and\n"
"hand them out to NGOs and other organizations and individuals that need\n"
"bridges.  Bridges that are distributed over the \"Reserved\" mechanism may not\n"
"see users for a long time.  Note that the \"Reserved\" distribution mechanism is\n"
"called \"Unallocated\" in %sbridge pool assignment%s files."
msgstr ""

#: bridgedb/strings.py:137
msgid "None"
msgstr "无"

#: bridgedb/strings.py:138
msgid ""
"Bridges whose distribution mechanism is \"None\" are not distributed by BridgeDB.\n"
"It is the bridge operator's responsibility to distribute their bridges to\n"
"users.  Note that on Relay Search, a freshly set up bridge's distribution\n"
"mechanism says \"None\" for up to approximately one day.  Be a bit patient, and\n"
"it will then change to the bridge's actual distribution mechanism.\n"
msgstr ""

#: bridgedb/strings.py:148
msgid "Please select options for bridge type:"
msgstr "请选择类型类型。"

#: bridgedb/strings.py:149
msgid "Do you need IPv6 addresses?"
msgstr "是否需要IPv6地址？"

#: bridgedb/strings.py:150
#, python-format
msgid "Do you need a %s?"
msgstr "是否需要 %s？"

#: bridgedb/strings.py:154
msgid "Your browser is not displaying images properly."
msgstr "浏览器无法正确显示图片。"

#: bridgedb/strings.py:155
msgid "Enter the characters from the image above..."
msgstr "请输入上图中的字符（不区分大小写）..."

#: bridgedb/strings.py:159
msgid "How to start using your bridges"
msgstr "如何使用网桥"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#: bridgedb/strings.py:161
#, python-format
msgid ""
" First, you need to %sdownload Tor Browser%s. Our Tor Browser User\n"
" Manual explains how you can add your bridges to Tor Browser. If you are\n"
" using Windows, Linux, or OS X, %sclick here%s to learn more. If you\n"
" are using Android, %sclick here%s."
msgstr "首先，您需要%s下载 Tor Browser%s。我们的 Tor Browser 用户手册解答了如何为 Tor Browser 添加您的网桥。如果您正在使用 Windows，Linux 或者 OS X，%s点击这里%s以详细了解。如果您正在使用安卓系统，%s点击这里%s。"

#: bridgedb/strings.py:166
msgid ""
"Add these bridges to your Tor Browser by opening your browser\n"
"preferences, clicking on \"Tor\", and then adding them to the \"Provide a\n"
"bridge\" field."
msgstr "打开你的浏览器偏好以添加这些网桥到你的Tor 浏览器中，点击“Tor”，然后将它们填入“提供一个网桥”一栏中。"

#: bridgedb/strings.py:173
msgid "(Request unobfuscated Tor bridges.)"
msgstr "（请求非混淆 Tor 网桥。）"

#: bridgedb/strings.py:174
msgid "(Request IPv6 bridges.)"
msgstr "(请求IPv6网桥。)"

#: bridgedb/strings.py:175
msgid "(Request obfs4 obfuscated bridges.)"
msgstr "（请求obfs4混淆网桥。）"
